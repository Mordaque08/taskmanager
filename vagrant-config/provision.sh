#!/usr/bin/env bash

echo "Provisioning ..."

# Set timezone
echo "Europe/Paris" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

# java 8 deb source
echo "deb http://http.debian.net/debian jessie-backports main" | tee /etc/apt/sources.list.d/mysql.list > /dev/null 2>&1
apt-get update > /dev/null 2>&1
apt-get install -y build-essential git unzip icedtea-7-plugin > /dev/null 2>&1


# Install Mysql
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install -y mysql-server mysql-client > /dev/null 2>&1
sed -i 's/127\.0\.0\.1/0\.0\.0\.0/g' /etc/mysql/my.cnf
mysql -uroot -proot -e 'USE mysql; UPDATE `user` SET `Host`="%" WHERE `User`="root" AND `Host`="localhost"; DELETE FROM `user` WHERE `Host` != "%" AND `User`="root"; FLUSH PRIVILEGES;' > /dev/null 2>&1
service mysql restart > /dev/null 2>&1

# Java 8 & Tomcat
apt-get install -y -t jessie-backports openjdk-8-jdk openjdk-8-jre > /dev/null 2>&1
update-java-alternatives --set java-1.8.0-openjdk-amd64 > /dev/null 2>&1
apt-get install -y tomcat8 tomcat8-admin tomcat8-examples tomcat8-docs > /dev/null 2>&1
cp /tmp/tomcat-users.xml /etc/tomcat8/tomcat-users.xml > /dev/null 2>&1
cp /tmp/setenv.sh /usr/share/tomcat8/bin > /dev/null 2>&1
service tomcat8 restart > /dev/null 2>&1
echo ""