package com.info921.sujet2.entities;

import javax.persistence.*;

@Entity
@Table(name = "test_table")
public class TestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "value")
    private String value;

    public int getId() {
        return id;
    }

    public TestEntity setId(int id) {
        this.id = id;
        return this;
    }

    public String getValue() {
        return value;
    }

    public TestEntity setValue(String value) {
        this.value = value;
        return this;
    }
}
