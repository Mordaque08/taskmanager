package com.info921.sujet2.api;

import com.info921.sujet2.entities.TestEntity;
import com.info921.sujet2.entities.dao.TestDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/test")
public class TestService {

    @Autowired
    private TestDAO testDAO;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<TestEntity> getAll() {
        return testDAO.getAll();
    }
}
