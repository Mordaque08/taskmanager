package com.info921.sujet2.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class Api {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String sayHtmlHello() {
        return "{\"response\": \"ok\"}";
    }
}
