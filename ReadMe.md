***Virtual Machine***

- debian jessie
- Apache Tomcat/8.0.14
- Java 1.7.0

first time init : ```vagrant init```

start vm: ```vagrant up```

stop vm : ```vagrant halt```

tomcat server address: ```http://192.168.33.101:8080```

tomcat admin address: ```http://192.168.33.101:8080/manager/```

admin user: ```admin:admin```

mysql info: ```192.168.33.101:3306```

mysql user: ```root:root```